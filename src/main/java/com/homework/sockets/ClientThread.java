package com.homework.sockets;

import java.io.*;
import java.net.Socket;

/**
 * Created by Valeriy on 30.01.2017.
 */
public class ClientThread extends Thread{
    private Socket clientSocket=null;
    ClientThread(Socket clientSocket){
        this.clientSocket=clientSocket;
    }

    @Override
    public void run() {
        InputStream inputStream;
        OutputStream os;
        DataInputStream dis=null;
        DataOutputStream dos=null;
        String string="";
        try {
                while (true) {
                    inputStream = clientSocket.getInputStream();
                    dis = new DataInputStream(inputStream);
                    string = dis.readUTF();
                    if(string.equals("exit")) {
                        System.exit(0);
                        break;}
                    System.out.println("Received from client: " + string);
                    for (Socket item : Server.clients) {
                        if (item == clientSocket) continue;
                        os = item.getOutputStream();
                        dos = new DataOutputStream(os);
                        dos.writeUTF(string);
                    }
                }
            }
        catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            try {
                if(string.equals("exit")){
                    assert dis != null;
                        dis.close();
                    assert dos != null;
                        dos.close();
                    clientSocket.close();
                    Server.clients.remove(clientSocket);
                }

            }
            catch (IOException e) {
                e.printStackTrace();
            }

        }
    }
}
