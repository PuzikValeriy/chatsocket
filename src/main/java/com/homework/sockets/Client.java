package com.homework.sockets;

import java.io.*;
import java.net.Socket;
import java.net.SocketException;
import java.util.Scanner;

/**
 * Created by Valeriy on 27.01.2017.
 */
public class Client {
    public static void main(String[] args) {
        Socket socket;
        Thread read;
        Thread write;
        try {
            socket = new Socket("127.0.0.1", 8888);
            Socket finalSocket = socket;
            read = new Thread(() -> {
                try {
                    InputStream inputStream = finalSocket.getInputStream();
                    DataInputStream dis = new DataInputStream(inputStream);
                        while (true) {
                            String string = dis.readUTF();
                            System.out.println(string);
                        }
                }
                catch (SocketException e){
                    System.out.println("Server disconnected");
                    System.exit(0);
                }
                catch (IOException e) {
                    e.printStackTrace();
                }
            });
            Socket finalSocket1 = socket;
            write = new Thread(() -> {
                try{
                    OutputStream outputStream = finalSocket1.getOutputStream();
                    DataOutputStream dos = new DataOutputStream(outputStream);
                    while (true) {
                        Scanner in = new Scanner(System.in);
                        String message = in.nextLine();
                        dos.writeUTF(message);
                        dos.flush();
                        if (message.equals("exit")) {
                            System.exit(0);
                            break;}
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
            read.start();
            write.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
