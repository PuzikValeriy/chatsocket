package com.homework.sockets;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Valeriy on 27.01.2017.
 */
public class Server {
    static final List<Socket> clients = new ArrayList<>();
    private static ServerSocket serverSocket=null;
    public static void main(String[] args) {
        ClientThread clientThread;
        try {
            serverSocket = new ServerSocket(8888);
            while (true){
                Socket clientSocket = serverSocket.accept();
                clientThread = new ClientThread(clientSocket);
                clientThread.start();
                clients.add(clientSocket);
                System.out.println(clients.size());
                System.out.println(clientSocket+" connected");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
